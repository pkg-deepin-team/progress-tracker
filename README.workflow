PACKAGES
--------

* Packages contain software from Deepin Company:
  + Managed by Git
  + Under /git/pkg-deepin/

* Packages maintained by pkg-deepin, act as dependency to deepin software:
  + Managed by Git (hopefully)
  + Under /git/collab-maint/

UPSTREAM CONTACT
----------------

There are several Deepin staff inside pkg-deepin team:

* leaeasy
* felixonmars
* yanhao

Contact them when in need. Official contact methods available too.

(https://www.deepin.org https://www.deepin.com)
